// This file is part of Smuxi and is licensed under the terms of MIT/X11
//
// Copyright (c) 2008 Mirco Bauer <meebey@meebey.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;

namespace Smuxi.Common
{
    public static class Defines
    {
        public const string GitBranch   = "release/1.1";
        public const string GitCommitHash = "b0476480";

        private static readonly string f_InstallPrefix = "/usr/local";
        private static readonly string f_DistVersion = "release/1.1/b0476480";
        private static readonly string f_TwitterApiKey = "60QV2qQx9cS7y1BJDbgAA|2VgD6qQKddsF5HYQ0TrRgs3tFTnCwDONBmRlTmG658";

        public static string InstallPrefix {
            get {
                return f_InstallPrefix;
            }
        }

        public static string TwitterApiKey {
            get {
                return f_TwitterApiKey;
            }
        }

        public static string GitVersion {
            get {
                if (String.IsNullOrEmpty(GitBranch) ||
                    String.IsNullOrEmpty(GitCommitHash)) {
                    return String.Empty;
                }

                return String.Format("{0}/{1}", GitBranch, GitCommitHash);
            }
        }

        public static string DistVersion {
            get {
                return f_DistVersion;
            }
        }
    }
}
